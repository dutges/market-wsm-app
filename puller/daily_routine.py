__author__ = 'dcortes'

from datetime import timedelta, date, datetime, time

from abstract_routine import abstract_routine
from celery_engine.client import Client

class DailyRoutine(abstract_routine):

    def __init__(self):
        """
        This routine will add the new values for the future day on mongo without checking
        :return:
        """
        abstract_routine.__init__(self)

    def _insert_concept(self, concept, reads, from_date):
        """
        First we need to look if the document is already on the collection if exists we just
        need to push the new values to the list values and sort them, if not we need to create
        the document
        # we use each and not pushall as the second one si just enable from 2.4 to future versions
        :param concept: the collection key
        :param reads: the reads to be append to the values key of the document
        :return:
        """
        query = {"month": from_date.strftime("%m-%Y")}
        concept = str(concept) + "s"
        if self.manager.get_document_exists(concept, query):
            new_fields = {"$push": {"values": {"$each": reads, "$sort": {"ts": 1}}}}
            self.manager.update_object(concept, new_fields, query)
        else:
            query["values"] = reads
            self.manager.insert_object(concept, query)

    def _insert_concepts(self, concepts, from_date, to_date):
        consolidation = {"status": False}
        for concept in concepts:
            try:
                concept = int(concept)
                reads = self._get_concept_readings(concept, from_date, to_date)
                if reads is not None and len(reads.get("indicator", {}).get("values", [])) > 0 :
                    reads = self._format_readings(reads["indicator"]["values"])
                    self._insert_concept(concept, reads, from_date)
                    consolidation["status"] = True
                else:
                    self.logger.warning("Problem taking data for concept %s for inteval [%s, %s]" % (concept,
                                                                                from_date.strftime("%m:%d"),
                                                                                to_date.strftime("%m:%d")
                                                                                ))

            except:
                self.logger.exception("not true concept")

        return consolidation

    def launch_tasks(self, consolidation, from_date, to_date):
        if consolidation["status"]:
            prices = self.manager.find_objects("prices", search_args={}, query_args={"_id":0})
            for price in prices:
                self.logger.info('Enqueue task with priceid = %s, depid = %s, from date = %s , to date = %s' %(str(price["dep_id"]),price['price'], from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')))
                Client.enqueue("celery_engine.tasks.calculate_price", args=[price["dep_id"], price["price"], from_date, to_date, 3,'importation'])

    def launch_insertion(self):
        tomorrow = date.today() + timedelta(days=1)
        from_date = datetime.combine(tomorrow, time.min)
        to_date = datetime.combine(tomorrow, time.max)
        self.logger.info('Starting Launch  Daily Price insertion From %s to %s ' % (from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')) )
        self.general_launch(from_date, to_date)
        self.logger.info('Daily insertion of task Done')


if __name__ == '__main__':
    dr = DailyRoutine()
    dr.launch_insertion()
