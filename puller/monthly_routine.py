__author__ = 'dcortes'

from datetime import date
from dateutil.relativedelta import relativedelta

from celery_engine.client import Client
from abstract_routine import abstract_routine


class MonthlyRoutine(abstract_routine):

    def __init__(self):
        abstract_routine.__init__(self)

    def get_hashed_reads(self, reads, key, value):
        values = dict()
        for read in reads:
            values[read[key]] = read[value]
        return values

    def _consolidation(self, reads_new, reads_old):
        """
        If some read differs from the old version saved on our bd then that means that the previous calculations need to be
        redone
        """
        for ts, read in reads_new.items():
            if reads_old.get(ts, 0) != read:
                return True
        return False

    def _insert_concept(self, concept, reads, from_date):
        """
        First we need to look if the document is already on the collection if exists we just
        need to push the new values to the list values and sort them, if not we need to create
        the document
        # we use each and not pushall as the second one si just enable from 2.4 to future versions
        :param concept: the collection key
        :param reads: the reads to be append to the values key of the document
        :return:
        """
        query = {"month": from_date.strftime("%m-%Y")}
        if self.manager.get_document_exists(concept, query):
            new_doc = {"month": from_date.strftime("%m-%Y"),"values": reads}
            self.manager.replace_object(concept, new_doc, query)
        else:
            query["values"] = reads
            self.manager.insert_object(concept, query)

    def _insert_concepts(self, concepts, from_date, to_date):
        consolidation = {"status": False, "concepts": []}
        for concept in concepts:
            try:
                self.logger.debug('Get information from Concept %s' % str(concept))
                concept = int(concept)
                reads = self._get_concept_readings(concept, from_date, to_date)
                if reads is not None and len(reads.get("indicator", {}).get("values", [])) > 0:
                    hash_reads = self.get_hashed_reads(reads["indicator"]["values"], "datetime", "value")
                    reads_saved = self.manager.find_object(str(concept)+'s', {"month": from_date.strftime("%m-%Y")})
                    if reads_saved is None or len(reads_saved.get("values",{})) == 0:
                        self.logger.debug('There is not readings saved in mongo from concept %s' %concept)
                        hash_reads_saved = {}
                    else:
                        hash_reads_saved = self.get_hashed_reads(reads_saved["values"], "ts", "v")
                    if self._consolidation(hash_reads, hash_reads_saved):
                        self.logger.debug('Doing Consolidation from concept %s' %str(concept))
                        reads = self._format_readings(reads["indicator"]["values"])
                        self._insert_concept(str(concept)+'s', reads, from_date)
                        consolidation["status"] = True
                        consolidation["concepts"].append(concept)
                    else:
                        self.logger.debug('There is not Consolidation from concept %s' %str(concept))
                else:
                    self.logger.warning("Problem taking data for concept %s for inteval [%s, %s]" % (concept,
                                                                                from_date.strftime("%m:%d"),
                                                                                to_date.strftime("%m:%d")
                                                                                ))
            except:
                self.logger.exception("not true concept")
        return consolidation

    def launch_tasks(self, consolidation, from_date, to_date):
        if consolidation["status"]:
            concepts = consolidation["concepts"]
            prices = self.manager.find_objects("prices", search_args={}, query_args={"dep_id": 1, "price": 1,"formula": 1, "_id": 0})
            for price in prices:
                if any(substring in concepts for substring in price.get("formula", "")):
                    Client.enqueue("celery_engine.tasks.calculate_price", args=[price["dep_id"], price["price"], from_date, to_date, 3,'consolidation'])

    def launch_insertion(self):
        from_date = date.today().replace(day=1) - relativedelta(months=1)
        to_date = date.today().replace(day=1) - relativedelta(seconds=1)
        self.logger.info('Starting Launch Monthly Price Consolidation From %s to %s ' % (from_date.strftime('%d/%m/%Y %H:%M:%S'),to_date.strftime('%d/%m/%Y %H:%M:%S')))
        self.general_launch(from_date, to_date)
        self.logger.info('Monthly consolidation Done')

if __name__ == '__main__':
    dr = MonthlyRoutine()
    dr.launch_insertion()