/*
 * Dexma core JS : JS core functions
 * 
 * Version 1.0
 *
 * Copyright (c) 2012 DEXMATECH (http://www.dexmatech.com/)
 *
 * Depends:
 *	jquery-XXX.js
 *	jquery-ui-XXX.custom.js
 *
 */ 

var DX = (function(){
	settings = {
			path : ""
	};	
	
	msg = {
			yes : "yes",
			no : "no",			
			deleteItemMsg : 'This item/s will be permanently deleted and cannot be recovered. Are you sure?',
			deleteItemTitle :'Confirm action',
			sun : 'Sun',
			sunday : 'Sunday',
			mon : 'Mon',
			monday : 'Monday',
			tue : 'Tue',
			tuesday : 'Tuesday',
			wed : 'Wed',
			wednesday : 'Wednesday',
			thu : 'Thu',
			thursday : 'Thursday',
			fri : 'Fri',
			friday : 'Friday',
			sat : 'Sat',
			saturday : 'Saturday',
			jan : 'Jan',
			january : 'January',
			feb : 'Feb',
			february : 'February',
			mar : 'Mar',
			march : 'March',
			apr : 'Apr',
			april : 'April',
			may : 'May',
			maylong : 'May',
			jun : 'Jun',
			june : 'June',
			jul : 'Jul',
			july : 'July',
			aug : 'Aug',
			august : 'August',
			sep : 'Sep',
			september : 'September',
			oct : 'Oct',
			october : 'October',
			nov : 'Nov',
			november : 'November',
			dec : 'Dec',
			december : 'December'
			
};	
return this;
}(DX));


DX.core = (function(){
	//private
	
	//public	
	return {	
	/**
	 * This function replace a url param with a new value
	 * 
	 * @param url
	 * @param paramName
	 * @param newValue
	 * @return url
	 */
	replaceUrlParam : function (url, paramName, newValue) {
	    var name = paramName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	    var regexS =  name + "=([^&#]*)";
	    var regex = new RegExp(regexS);
	    url = url.replace(regex, name+'='+newValue);
	   return url;
	},

	checkLength : function ( o,  min, max ) {
		if ( o.length > max || o.val().length < min ) {			
			return false;
		} else {
			return true;
		}
	},
	
	checkRegexp : function ( o, regexp ) {
		if ( !( regexp.test( o ) ) ) {			
			return false;
		} else {
			return true;
		}
	},
	
	checkEmail : function ( email ) {

		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9_\.\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email)) {		
			return false;
		}
		return true;
	},
	
	checkHasInvalidCharacters : function ( name ) {
	    if(name.indexOf("&") != -1 || name.indexOf("\"") != -1 || name.indexOf("\'") != -1 || name.indexOf("<") != -1 || name.indexOf(">") != -1) {  
	 		return true;
    	}
		return false;
	},


	changeLocale : function (locale){
		var currentURL = document.URL;
		if(currentURL.search(/siteLanguage/i)!=-1){
			window.location.href = DX.core.replaceUrlParam(currentURL,'siteLanguage',locale);
		}else{
			if(document.URL.indexOf("?") != -1){
				currentURL = currentURL + "&siteLanguage=";
			}else{
				currentURL = currentURL + "?siteLanguage=";
			}
			window.location.href=currentURL+locale;
		}
		
	}
	}
}());




function exportAsImage(type, filename){
	var flashMovie = document.getElementById(type);   //I used a ampie chart
	if (flashMovie) { 
			flashMovie.exportImage('http://www.dexmatech.com/export.php?filename='+filename);   
	} 
}