/*
 * Dexma UI JS middleware: Adapters for JS UI frameworks
 * 
 * Version 1.0
 *
 * Copyright (c) 2012 DEXMATECH (http://www.dexmatech.com/)
 *
 * Depends:
 *  dexcell-core.js
 *	jquery-XXX.js
 *	jquery-ui-XXX.custom.js
 *  jquery.format.1.05.js
 *  jquery.datepicker-dexma.js
 *
 */ 

// init 



DX.ui = (function(){

//private
	
//public	
return {	
	
	tabulatorKeyPressed  : function(event) {
			if ((event.which && event.which == 9) || (event.keyCode && event.keyCode == 9)) {s
				return true;
			}
			return false;
	},
	
	carryKeyPressed  : function(event) {
		if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {
			return true;
		}
		return false;
	},
	dialogSubmitOn13Key  : function() {
		$('.ui-dialog').find('input').keypress(function(e) {
			if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
				$('.ui-dialog').find('button:first').click(); /* Assuming the first one is the action button */
				return false;
			}
		});
	},
	notification : function (msg,type) {
		var timeout = false;
		if(type == 'success' || type == 'error' ||type == 'warning' || type == 'information') {	
			if(type == 'success' ||  type == 'information') timeout = '5000';
			else if (type == 'warning') timeout = '15000';
			noty({
				text: msg,
				type: type,
				template: '<div class="noty_message noty_'+type+'"><div class="noty_text"></div></div>',
				dismissQueue: true, 
				timeout:timeout,
				layout: 'topRight',
				 animation: {
					    open: {height: 'toggle'},
					    close: {height: 'toggle'},
					    easing: 'swing',
					    speed: 500 // opening & closing animation speed
					  }
				});
			}
		
	},
	
	
	message : function(title ,content){
		 var newDialog = $('<div id="alert-dexma-div"><p>'+content+'</p></div>');
		 newDialog.dialog({
				resizable: false,
				title:title,
				modal: true,
				open: function () {
					DX.ui.transformDialogButtons();
				},
				buttons: [ {
		              text: "OK",
		              click: function() { 
		            	  $( this ).dialog( "destroy" );}
		          }] 
		});
		 
	},
	
	warn : function(title ,content){
		 var newDialog = $('<div id="alert-dexma-div"><div style="padding: 1em 1.75em;"><p>'+content+'</p><div></div>');
		 newDialog.dialog({
				resizable: false,
				title:'<span class="ui-icon ui-icon-alert" style="float:left;margin-right: 4px;"></span>'+title,
				modal: true,
				open: function () {
					DX.ui.transformDialogButtons();
				},
				buttons: [ {
		              text: "OK",
		              click: function() { 
		            	  $( this ).dialog( "destroy" );}
		          }] 
		});
	},
	
	info : function(title ,content){
		 
		 var newDialog = $('<div id="alert-dexma-div"><div style="padding: 1em 1.75em;"><p>'+content+'</p><div></div>');
		 newDialog.dialog({
				resizable: false,
				title:'<span class="ui-icon ui-icon-info" style="float:left;margin-right: 4px;"></span>'+title,
				modal: true,
				open: function () {
					DX.ui.transformDialogButtons();
				},
				buttons: [ {
				              text: "OK",
				              click: function() { 
				            	  $( this ).dialog( "destroy" );}
				          }] 
		});
	},
	
	/**
	 * Open a modal dialog to and wait until a cookie is setted by server
	 * 
	 * @param url
	 */
	showDownloadDialog : function(){
		var conf = { checkInterval : 100,  
		          cookieName : "fileDownload",
		          cookieValue: "true",
		          cookiePath: "/"
			};
		 var newDialog = $('<div id="download-div" title="Info">Please wait, downloading  ... <div style="padding-top:10px;width:100%;" class="center"><img src="'+settings.path+'/img/animations/ajax-loader.gif"/></div></div>');
		 newDialog.dialog({resizable: false,modal: true});
		 
		var loop = window.setInterval(function () {
			if (document.cookie.indexOf(conf.cookieName + "=" + conf.cookieValue) != -1) {
				DX.ui.closeAllPopups();
				var date = new Date(1000);
				document.cookie = conf.cookieName + "=; expires=" + date.toUTCString() + "; path=" + conf.cookiePath;
				window.clearInterval(loop);
			}
		}, 1000);
		
	},
		
	
	/**
	 * Open a modal dialog to confirm an action
	 * 
	 * @param url
	 */
	confirmAction : function(url, title, msg, buttonConfirmMsg, buttonCancelMsg ){
		  
		 if(title == null || typeof(title) == "undefined") {
			 title = DX.msg.deleteItemTitle;
		 }
		 if(msg == null || typeof(msg) == "undefined") {
			 msg = DX.msg.deleteItemMsg;	  
		 }
		 if(buttonConfirmMsg == null || typeof(buttonConfirmMsg) == "undefined") {
			 buttonConfirmMsg = DX.msg.yes;  
		 }
		 if(buttonCancelMsg == null || typeof(buttonCancelMsg) == "undefined") {
			 buttonCancelMsg = DX.msg.no;  
		 }
		 var newDialog = $('<div id="confirm-div" style="min-height: 80px;padding: 1em 1.75em"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p></div>');	
		 newDialog.dialog({
				resizable: true,
				//height:200,
				title: title,
				modal: true,
				open: function () {
					DX.ui.transformDialogButtons();
				},
				buttons: [
				          {
				              text: buttonConfirmMsg,
				              click: function() { 
				            	  $( this ).dialog( "destroy" );				              
								   document.location.href =url;}
				          }, {
				              text: buttonCancelMsg,
				              click: function() { $( this ).dialog( "destroy" ); }
				          }
				          
				      ] 
			});
		 $( 'a.ui-dialog-titlebar-close' ).remove();
		 newDialog.bind("dialogclose", function(event, ui) {newDialog.dialog("destroy").empty();newDialog.remove();});
	},
	popupAction : function(id,funOk, title, buttonConfirmMsg, buttonCancelMsg, width){
		  $('#'+id).dialog({
				resizable: false,
			/*	height:200,*/
				title: title,
				modal: true,
				open: function () {
					DX.ui.transformDialogButtons();
				},
				buttons: [
				          {
				              text: buttonConfirmMsg,
				              click: function() { 
				            	  funOk();
				            	  $( this ).dialog( "destroy" );				              
						   }
				          }, {
				              text: buttonCancelMsg,
				              className: 'btn',
				              click: function() { $( this ).dialog( "destroy" ); }
				          }
				          
				      ] 
			});
		if(width){
			$('#'+id).dialog("option", "width", width );
		}
	},
	
	transformDialogButtons : function () {
		$('.ui-dialog-buttonpane').find('button').each(function (){
				$(this).removeClass();
				$(this).addClass('btn');
			});
	},
	
//	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
	/**
	 * Open a modal dialog to confirm an ajax action
	 * 
	 * @param url
	 */
	confirmActionAjax : function(url, title, msg, buttonConfirmMsg, buttonCancelMsg){
		
			if(title == null || typeof(title) == "undefined") {
				 title = DX.msg.deleteItemTitle;
			 }
			 if(msg == null || typeof(msg) == "undefined") {
				 msg = DX.msg.deleteItemMsg;	  
			 }
			 if(buttonConfirmMsg == null || typeof(buttonConfirmMsg) == "undefined") {
				 buttonConfirmMsg = DX.msg.yes;  
			 }
			 if(buttonCancelMsg == null || typeof(buttonCancelMsg) == "undefined") {
				 buttonCancelMsg = DX.msg.no;  
			 }	
			 var newDialog = $('<div id="confirm-div" style="min-height: 80px;padding: 1em 1.75em"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p></div>');	
			 newDialog.dialog({
				resizable: false,
				height:200,
				open: function () {
					DX.ui.transformDialogButtons();
				},
				title: title,
				modal: true,
				buttons: [
				          {
				              text: buttonConfirmMsg,
				              click: function() { 			            					              
				            	  $.ajax({ url: url,
				 				     async: true,		  		  				     
				 				 	 error: function(error) {alert("ajaxError");}
				 				});
				            	  result = true;
				            	  $( this ).dialog( "destroy" );   
				              }
				          }, {
				              text: buttonCancelMsg,
				              click: function() { $( this ).dialog( "destroy" ); }
				          }
				          
				      ] 
			});
			 newDialog.bind("dialogclose", function(event, ui) {newDialog.dialog("destroy").empty();newDialog.remove();});
	},
	/**
	 * Open a modal dialog to confirm an ajax action and call function
	 * 
	 * @param url
	 */
	confirmActionFunctionAjax : function(fun, url, title, msg, buttonConfirmMsg, buttonCancelMsg){
		 if(title == null || typeof(title) == "undefined") {
			 title = DX.msg.deleteItemTitle;
		 }
		 if(msg == null || typeof(msg) == "undefined") {
			 msg = DX.msg.deleteItemMsg;	  
		 }
		 if(buttonConfirmMsg == null || typeof(buttonConfirmMsg) == "undefined") {
			 buttonConfirmMsg = DX.msg.yes;  
		 }
		 if(buttonCancelMsg == null || typeof(buttonCancelMsg) == "undefined") {
			 buttonCancelMsg = DX.msg.no;  
		 }	
		 var newDialog = $('<div id="confirm-div" style="min-height: 80px;padding: 1em 1.75em"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+msg+'</p></div>');	
		 newDialog.dialog({
				resizable: false,
				height:300,
				title: title,
				open: function () {
					DX.ui.transformDialogButtons();
				},
				modal: true,
				buttons: [
				          {
				              text: buttonConfirmMsg,
				              click: function() {
				            	  if(url != null && typeof(url) != "undefined") {
				            		  $.ajax({ url: url,
						 				     async: false,		  		  				     
						 				 	 error: function(error) {alert("ajaxError");}
						 				});
				         		  }
				            	  
				            	  fun();
				            	  $( this ).dialog( "destroy" );   
				              }
				          }, {
				              text: buttonCancelMsg,
				              click: function() { $( this ).dialog( "destroy" ); result = false;}
				          }
				          
				      ] 
			});
		 newDialog.bind("dialogclose", function(event, ui) {
		 	newDialog.dialog("destroy").empty();
		 	newDialog.remove();
	 	 });
		 
	},
	
	/**
	 * Open a modal dialog to confirm an ajax action and call function
	 * 
	 * @param url
	 */
	marketAppPopUp : function(url, title, msg, buttonConfirmMsg, buttonCancelMsg){
		 if(title == null || typeof(title) == "undefined") {
			 title = DX.msg.deleteItemTitle;
		 }
		 if(msg == null || typeof(msg) == "undefined") {
			 msg = DX.msg.deleteItemMsg;	  
		 }
		 if(buttonConfirmMsg == null || typeof(buttonConfirmMsg) == "undefined") {
			 buttonConfirmMsg = DX.msg.yes;  
		 }
		 if(buttonCancelMsg == null || typeof(buttonCancelMsg) == "undefined") {
			 buttonCancelMsg = DX.msg.no;  
		 }	
		 var newDialog = $('<div id="marketAppPopUp"><p>' + msg + '</p></div>');	
		 newDialog.dialog({
				resizable: true,
				title: title,
				modal: true,
				open: function () {
					DX.ui.transformDialogButtons();
				},
				buttons: [
				          {
				              text: buttonConfirmMsg,
				              click: function() { 
				            	  $(this).dialog( "destroy" );				              
								   document.location.href =url;}
				          }, {
				              text: buttonCancelMsg,
				              click: function() { $(this).dialog( "destroy" ); }
				          }
				          
				      ] 
			});
		 $( 'a.ui-dialog-titlebar-close' ).remove();
		 newDialog.bind("dialogclose", function(event, ui) {newDialog.dialog("destroy").empty();newDialog.remove();});
	},
	
	/**
	 * Open a PopUp Ajax 
	 * 
	 * @param title
	 * @param urlAjax
	 * @param idDiv
	 */
	popupAjax : function(title, urlAjax, id){
		 var newDialog;
		 if(id != null || typeof(id) != "undefined") {
			 $('#'+id).html("<div id='pop-up-ajax' style='text-align: center;width: 150px;height: 120px;'><img style='margin-top:30px;margin-botom:30px;' src='"+DX.settings.path+"/img/common/loading_1.gif'/></div>");
			 newDialog =  $('#'+id).dialog( 
				  		{title: title,
					  		 resizable: false,
					  		 dragable:false,
					  		 bgiframe: true,
					  		 cache:false,
					         modal: true,
					         autoOpen: false,
					         width:'auto'
					  		});
		 } else {
			 newDialog = $("<div id='pop-up-ajax' style='text-align: center;width: 150px;height: 120px;'><img style='margin-top:30px;margin-botom:30px;' src='"+DX.settings.path+"/img/common/loading_1.gif'/></div>");
			 newDialog.dialog( 
				  		{title: title,
				  		 resizable: false,
				  		 dragable:false,
				  		 bgiframe: true,
				  		 cache:false,
				         modal: true,
				         autoOpen: false,
				         width:'auto'
				  		}); 
		 }
		
			
			newDialog.dialog('open');
			$.ajax({
			  url: urlAjax,
			  cache: false,
			  resizable: false,
			  async: true,		  		  
			  success: function(data) {	
				  newDialog.dialog('close');
				  $('#pop-up-ajax').css('text-align','left');
				  newDialog.html(data);
				  newDialog.dialog('open');
				  if(id == null || typeof(id) == "undefined") {
					  newDialog.bind("dialogclose", function(event, ui) {newDialog.dialog("destroy").empty();newDialog.remove();});
				  }
				  $('.jquery-bug').hide();
			  },			
			  error: function(error) {
				newDialog.dialog('close');
				alert("Error de llamada ajax");
		  	  }
			});
			
	 },
	 popup : function(idDiv,title){
		  $('#'+idDiv).dialog({
			         title: title,
			  		 resizable: true,
			  		 dragable:true,
			  		 bgiframe: true,
			         modal: true,
			         autoOpen: false,
			         open: function () {
							DX.ui.transformDialogButtons();
						},
			         width:'auto'
			  		});
		  $('#'+idDiv).dialog('open');			
	 },
	 popupEle : function($ele){		  			 
		 $ele.dialog({
			  		 resizable: true,
			  		 dragable:false,
			  		 bgiframe: true,
			         modal: true,
			         autoOpen: false,
			         open: function () {
							DX.ui.transformDialogButtons();
						},
			         width:'auto'
			  		});
		 $ele.dialog('open');			
	 },
	 popupURL : function(idDiv, url){		  			 
		  $('#'+idDiv).dialog({
			  		 resizable: true,
			  		 dragable:false,
			  		 bgiframe: true,
			         modal: true,
			         autoOpen: false,
			         width:800,
			         height:800
			  		});
			  		
		  $('#'+idDiv).dialog('open').append($("<iframe />").attr("src", url).attr("width", "100%").attr("height", "99%"));			
	 },
	/**
	* Close all current popups opened
	* 
	*/
	closeAllPopups : function(){
		 $(".ui-dialog-content").dialog('close');
	},
	/**
	* Close popup with id
	* 
	* @param id
	*/
	closePopup : function(id){
		$('#'+id).dialog('close');
	},
	
	preventHrefScroll : function() {
		$("a[href='#']").click(function (event) {
		    event.preventDefault();
		});
	},
	 
	datepickerSetDefaults : function(){
		$.datepicker.setDefaults({ dateFormat : 'dd/mm/yy' });
	},
	/**
	* Open jquery datepicker on a input element 
	* 
	* @param id
	* @param fun function triggered on select date
	*/
	datepicker : function(id,fun){
		$("#"+id).datepicker({
			dateFormat : 'dd/mm/yy',
			changeMonth: true,
			changeYear: true,
			showAnim:'slideDown',
			buttonImage: DX.settings.path+"static/img/datepicker_button.png",
			showOn: "both",
			showButtonPanel: true,
			buttonImageOnly: true,
			onSelect: function(dateText, inst) {	
				if(fun != null){
					fun(dateText, inst);
				}
			}}).change(function() {
				if(fun != null){
					fun();
				}
			});
	},
	
	/**
	* Open jquery datepicker on a input element 
	* 
	* @param id
	* @param fun function triggered on select date
	*/
	datepickers : function(cssClass, idParent){
		if(idParent) {
			$('#'+idParent).find("."+cssClass).datepicker({
				dateFormat : 'dd/mm/yy',
				changeMonth: true,
				changeYear: true,
				showAnim:'slideDown',
				buttonImage: DX.settings.path+"/img/datepicker_button.png",
				showOn: "both",
				showButtonPanel: true,
				buttonImageOnly: true
			});
		} else {
			$("."+cssClass).datepicker({
				dateFormat : 'dd/mm/yy',
				changeMonth: true,
				changeYear: true,
				showAnim:'slideDown',
				buttonImage: DX.settings.path+"/img/datepicker_button.png",
				showOn: "both",
				showButtonPanel: true,
				buttonImageOnly: true
			});
		}
	},
	
	/**
	* 	Creates a jquery date range picker in a div (idDivRangedatepicker), getting and settin dates from external inputs
	* 
	* @param idInputFrom
	* @param idInputTo 
	* @param idDivRangedatepicker function triggered on select date
	*/
	rangedatepick : function(idInputFrom, idInputTo, idDivRangedatepicker) {
		var $idDivRangedatepicker = $('#'+idDivRangedatepicker);
		var $from = $('#'+idInputFrom);
		var $to = $('#'+idInputTo);
		
		var fromDate = $from.val();
		var toDate = $to.val();
		var current;
		
		$idDivRangedatepicker.html('');
		$idDivRangedatepicker.append('<div id="date-range-field"><span></span></div>');
		$idDivRangedatepicker.append('<div id="datepicker-calendar" style="display:none;position:relative;"></div>');
		
		if(fromDate!=null && toDate!= null && fromDate!='' && toDate!= '') {
			$('#date-range-field span').text(fromDate+ ' - ' +toDate);
		
			fromDate = Date.parseExact(fromDate, 'd/M/yyyy');

			toDate = Date.parseExact(toDate, 'd/M/yyyy');
			current = fromDate;
		} else {
			$('#date-range-field span').text('? - ?');
			current = new Date();
		}
		$('#datepicker-calendar').DatePicker({
		    inline: true,
		    date: [fromDate, toDate],
		    calendars: 3,
		    mode: 'range',
		    starts: 1,
		    current: current,
		    showRanges: true,
		    textField : '#date-range-field',
		    onApply: function () {	
		    	$from.val($('#datepicker-calendar').DatePickerGetDate()[0][0].toString('dd/MM/yyyy'));
		    	$to.val($('#datepicker-calendar').DatePickerGetDate()[0][1].toString('dd/MM/yyyy'));
		    	 $('#datepicker-calendar').toggle();
		    	 $('#date-range-field').toggleClass('opened');
		    }
		});
		$('#date-range-field').bind('click', function (){
			 $('#datepicker-calendar').toggle();
			 $('#date-range-field').toggleClass('opened');
		});
		
	},
	
	/**
	* Apply format plugin to all input type='text' with class '.price' 
	* 
	* @param precision number of decimals
	*/
	priceFormat : function (precision) {
		$(".price").format({precision: precision,allow_negative:false,autofix:true});
	},

	/**
	* Apply format plugin to all input type='text' with class '.price' 
	* 
	* @param precision number of decimals
	*/
	priceFormatNeg : function (precision) {
		$(".price_neg").format({precision: precision,allow_negative:true,autofix:true});
	},
	/**
	* Apply format plugin to all input type='text' with class className 
	* 
	* @param precision number of decimals
	* @param className class to apply
	*/
	numberFormat : function (precision, className) {
		$("."+className).format({precision: precision,allow_negative:false,autofix:true});
	},
	/**
	* Apply format plugin to all input type='text' with class '.price_integer' 
	* 
	*/
	integerFormat : function () {
		$(".price_integer").format({precision: 0,allow_negative:false,autofix:true});
	},
	
	
	/**
	* Transform inputs type button in jquery style
	* 
	*/
	renderizeCheckboxes : function(){
		$('input[type="checkbox"]:checked').parent(".checkbox").addClass("checked");
		
		$('input[type="checkbox"]')
		.change(function() {
		    if($(this).is(':checked')) {
		        $(this).parent(".checkbox").addClass("checked");
		    }
		    else {
		        $(this).parent(".checkbox").removeClass("checked");
		    }
		})
		.mousedown(function() {
		    $(this).parent(".checkbox").addClass("active");
		})
		.mouseup(function() {
		    $(this).parent(".checkbox").removeClass("active");
		});
		
		
	},
	
	/**
	* Transform inputs type radio in ezMark style
	* 
	*/
	renderizeRadios : function(){
		  $('input[type="radio"]').ezMark();
	
	},
	
	/**
	* Transform inputs type select in select2 style by class name
	*
	* @param cssClass class name 
	*/
	renderizeSelects : function(cssClass){
		$('.'+cssClass).each(function (){
			try  {
				$(this).select2({
			        minimumResultsForSearch : 99999
			    });
			  }
			catch(err) {
			  //Handle errors here
			  }
		});		
	},
	
	/**
	* Transform single input type select in select2 style
	*
	* @param idHtml html identifier 
	* @param value init value 
	*/
	renderizeSelect : function(idHtml, value){	
			 $('#'+idHtml).select2({
			        minimumResultsForSearch : 99999
			    });	
			 if(value!=null) {
				 $('#'+idHtml).select2("val", value);
			 }
	},
	
	
	renderizeFormErrors : function () {
		$('.form_error').each(function (){
 			var $error = $(this);
 			var contentHtml = $error.html();
 			$error.empty();
 			$error.qtip({
 				content: contentHtml,
 				position: {
 		              corner: {
 		                 tooltip: 'leftMiddle', // Use the corner...
 		                 target: 'rightMiddle' // ...and opposite corner
 		              }
 		           },
 		           style: {
 		        	 /*  fontFamily: 'trebuchet MS',*/
 		        	  background: '#D63333',
 		        	  color: 'white',
 		              border: {
 		                 width:1,
 		                 radius: 4,
 		                 color: '#D63333',	                 
 		              },
 		              padding: 2, 
 		              textAlign: 'center',
//  		              tip: true,
 		              tip: { corner: 'leftMiddle',size: { x: 5, y: 5}}
 		           }
 			});
 		});
	},
	
	
	/**
	* Clear selects multiple
	*
	* @param cssClass class name 
	*/
	clearSelects : function (cssClass) {
		$('.'+cssClass).select2("data", "");
        $('.'+cssClass).html('');
	},
	
	/**
	* Clear single select multiple
	*
	* @param idHtml html identifier 
	*/
	clearSelect : function (idHtml) {
		$('#'+idHtml).select2("data", "");
        $('#'+idHtml).html('');
	},

	/**
	* Transform multiselect options
	*
	* @param idHtml html identifier 
	*/
	initMultiSelect : function (idHtml) {
		var selectedOptions = [];
		$('#'+idHtml).find('option').each(function () {
			if($(this).attr('selected')) {
				selectedOptions.push($(this).val());
			}
		});
		$('#'+idHtml).select2("val", selectedOptions);
	},
	
	/**
	* Toggle css class events in array of html elements by parent element
	*
	* @param idHtml html identifier 
	* @param toogleClass css clas to toggle 
	*/
	toggleClass : function (idParent, toogleClass) {
		$('#'+idParent).children().click(function(){
			$('#'+idParent).children().removeClass(toogleClass);
			$(this).addClass(toogleClass);
		});
	},
	
	
	/**
	* Transform inputs type button in jquery style
	* 
	*/
	selectAllTable : function(checkInput, checkAttr){
	
			  $(checkInput).on('click',function(){
			    if ($(this).parent().hasClass('checked')) {
			      $(checkAttr).attr('checked', true);
			      $(checkAttr).parent().addClass('checked');
			    } else {
			      $(checkAttr).attr('checked', false);
			      $(checkAttr).parent().removeClass('checked');
			    }
			  });
	},
	
	
	
	/**
	* Transform inputs type button in jquery style
	* 
	*/
	renderizeDropdowns : function(){
		!function ($) {

			  "use strict"; // jshint ;_;


			 /* DROPDOWN CLASS DEFINITION
			  * ========================= */

			  var toggle = '[data-toggle="dropdown"]'
			    , Dropdown = function (element) {
			        var $el = $(element).on('click.dropdown.data-api', this.toggle)
			        $('html').on('click.dropdown.data-api', function () {
			          $el.parent().removeClass('open')
			        })
			      }

			  Dropdown.prototype = {

			    constructor: Dropdown

			  , toggle: function (e) {
			      var $this = $(this)
			        , $parent
			        , selector
			        , isActive

			      if ($this.is('.disabled, :disabled')) return

			      selector = $this.attr('data-target')

			      if (!selector) {
			        selector = $this.attr('href')
			        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
			      }

			      $parent = $(selector)
			      $parent.length || ($parent = $this.parent())

			      isActive = $parent.hasClass('open')

			      clearMenus()

			      if (!isActive) $parent.toggleClass('open')

			      return false
			    }

			  }

			  function clearMenus() {
			    $(toggle).parent().removeClass('open')
			  }


			  /* DROPDOWN PLUGIN DEFINITION
			   * ========================== */

			  $.fn.dropdown = function (option) {
			    return this.each(function () {
			      var $this = $(this)
			        , data = $this.data('dropdown')
			      if (!data) $this.data('dropdown', (data = new Dropdown(this)))
			      if (typeof option == 'string') data[option].call($this)
			    })
			  }

			  $.fn.dropdown.Constructor = Dropdown


			  /* APPLY TO STANDARD DROPDOWN ELEMENTS
			   * =================================== */

			  $(function () {
			    $('html').on('click.dropdown.data-api', clearMenus)
			    $('body')
			      .on('click.dropdown', '.dropdown form', function (e) { e.stopPropagation() })
			      .on('click.dropdown.data-api', toggle, Dropdown.prototype.toggle)
			  })

			}(window.jQuery);
	},

	/**
	* Init initOnOffSwitch components
	*
	*/
	initOnOffSwitch : function(){	
		 // Switch activated
	    $('.onoffswitch').click(function () {
	        var $checkbox = $(this).find(':checkbox');
	        $checkbox.attr('checked', !$checkbox.is(':checked'));
	    });
	}
	
}	

}());