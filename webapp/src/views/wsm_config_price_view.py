# -*- coding: utf-8 -*-
__author__ = 'dcortes'

from flask import Blueprint, render_template, current_app, request, jsonify

from driver_management.redis_managements import redis_management
from driver_management.mongo_management import mongo_management
from driver_management.http_management import http_management
from ..controller.wsm_config_price import wsm_config_price
import json
mod_wsm_config_price = Blueprint('price_config', __name__)




@mod_wsm_config_price.route('/<int:dep_id>/new', methods=['POST'])
def wsm_save_config(dep_id):
    """
        Principal call, just return the main page.
    """
    try:
        current_app.logger.info('moving to configuration price view')
        current_app.logger.info(current_app.root_path)
        price = request.form.get("price")
        price_info = {
            "dep_id": dep_id,
            "price": price,
            "concepts": request.form.getlist("concepts[]"),
            "formula": request.form.get("formula"),
            "periods": request.form.get("periods"),
            "tariff_structure": request.form.get("ts"),
            "other_concepts": json.loads(request.form.get("other_concepts")),
            "last_importation": request.form.get("last_importation"),
            "last_consolidation": request.form.get("last_consolidation"),
        }
        managers = current_app.config['managers']
        controller = wsm_config_price(dep_id, managers, current_app.config)
        if (controller.exist_price(dep_id, price)):
            status = 'Exists Price'
        else:
            status = controller.insert_price(price_info)
        return jsonify(status=status)
    except:
        current_app.logger.exception("Problem loading view")
        return render_template("errors/500.html")

@mod_wsm_config_price.route('/<int:dep_id>/edit/<string:price_old>', methods=['POST'])
def wsm_edit_config(dep_id,price_old):
    '''
    Call to edit a price
    :return:
    '''
    try:
        current_app.logger.info('Edit a Price configuration')
        price = request.form.get("price")
        price_info = {
            "dep_id": dep_id,
            "price": price,
            "concepts": request.form.getlist("concepts[]"),
            "formula": request.form.get("formula"),
            "periods": request.form.get("periods"),
            "tariff_structure": request.form.get("ts"),
            "other_concepts": json.loads(request.form.get("other_concepts")),
            "last_importation": request.form.get("last_importation"),
            "last_consolidation": request.form.get("last_consolidation")
        }
        managers = current_app.config['managers']
        controller = wsm_config_price(dep_id, managers, current_app.config)
        if price_old != price:
            if (controller.exist_price(dep_id, price)):
                status = 'Exists Price'
                return jsonify(status=status)
        controller = wsm_config_price(dep_id, managers, current_app.config)
        status = controller.update_price(price_info,price_old)
        return jsonify(status=status.matched_count)
    except:
      current_app.logger.exception("Problem loading view")
      return render_template("errors/500.html")

@mod_wsm_config_price.route('/<int:dep_id>/price/<string:price_id>', methods=['GET'])
def wsm_price_page(dep_id,price_id):
    '''
    giving the information from one price configuration
    :return:
    '''

    try:
        managers = current_app.config['managers']
        controller = wsm_config_price(dep_id, managers, current_app.config)
        wsm_price = controller.get_price_config(dep_id, price_id)
        return jsonify(wsm_price=wsm_price)
    except:
        current_app.logger.exception("Problem loading view")
        return render_template("errors/500.html")

