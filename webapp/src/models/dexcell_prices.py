__author__ = 'jnegrete'

import logging
from logging import NullHandler,StreamHandler
import sys

class dexcellPrices():

    def __init__(self, http_management, token, logger_name, endpoint):
        self.http_management = http_management
        self.headers = {"x-dexcell-token": token}
        self._get_logger()
        self.endpoint = endpoint

    def _get_logger(self):
        self.logger = logging.getLogger()
        if len(self.logger.handlers) == 0:
            self.logger.setLevel(logging.INFO)
            self.handler = StreamHandler(sys.stdout)
            h_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            self.handler.setFormatter(logging.Formatter(h_format))
            self.logger.addHandler(self.handler)

    def get_dexcell_prices(self, type_price="ELECTRICITY"):
        """
        this function get from Dexcell Api all the prices configured for the deployment implicit in the
        token
        :return:
        """
        parameters = {"limit": 500, "start": 0}
        prices = self.http_management.call_rest_get(self.endpoint, "/utility/prices/%s" % type_price, self.logger, headers=self.headers,params=parameters)
        while len(prices) == 500:
            parameters["start"] += 1
            some_prices =  self.http_management.call_rest_get(self.endpoint, "/utility/prices/%s" % type_price, self.logger, headers=self.headers,params=parameters)
            prices.extend(some_prices)
        return prices
