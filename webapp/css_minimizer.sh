#!/bin/sh
css_files=`find /home/dcortes/PycharmProjects/wsm/webapp/static/css -name "*.css"`
rm  /home/dcortes/PycharmProjects/wsm/webapp/static/css/style-compact-min.css
touch /home/dcortes/PycharmProjects/wsm/webapp/static/css/style-compact-min.css
for file in $css_files
do
    if [ "$file" != "/home/dcortes/PycharmProjects/wsm/webapp/static/css/style-compact-min.css" ]
        then
        echo "Compressing $file …"
        yuicompressor $file > $file.min.css
        cat $file.min.css >> /home/dcortes/PycharmProjects/wsm/webapp/static/css/style-compact-min.css
        rm $file.min.css
    fi
done
