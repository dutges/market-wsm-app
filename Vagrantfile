# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

$bootstrap_redis = <<-SHELL

    echo "Installing Redis v 2.8.4"
    mkdir /opt/redis
    mkdir /opt/redis/bin

    cd /opt/redis
    wget http://download.redis.io/releases/redis-2.8.4.tar.gz >/dev/null 2>&1
    tar xzf redis-2.8.4.tar.gz >/dev/null 2>&1

    cd redis-2.8.4
    make >/dev/null 2>&1
    make install >/dev/null 2>&1

    cd /opt/redis/redis-2.8.4/utils
    echo -n | ./install_server.sh >/dev/null 2>&1

    echo "Enabling default password for redis foobared"
    sed -i  's/# requirepass foobared/requirepass foobared/g' /etc/redis/6379.conf

    echo "Start redis service"
    service redis_6379 stop >/dev/null 2>&1
    service redis_6379 start >/dev/null 2>&1

SHELL

$bootstrap_mongo = <<-SHELL
    # Add MongoDB repository.

    echo "Installing mongodb"
    add-apt-repository "deb http://downloads-distro.mongodb.org/repo/debian-sysvinit dist 10gen"
    # Add key for MongoDB repository.
    apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
    aptitude update >/dev/null 2>&1

    apt-get install -y mongodb-org-server=2.6.10 mongodb-org-shell=2.6.10 mongodb-org-tools=2.6.10 >/dev/null 2>&1

    # Pin packages to prevent unwanted upgrades.
    echo "mongodb-org-server hold" | sudo dpkg --set-selections
    echo "mongodb-org-shell hold" | sudo dpkg --set-selections
    echo "mongodb-org-tools hold" | sudo dpkg --set-selections

    export LC_ALL=C

    #Open MongoDB connections to the outside world
    sed -i 's/bind_ip/#bind_ip/g' /etc/mongod.conf

    echo "Start mongodb service"
    #Restart everything
    service mongod restart >/dev/null 2>&1

    #Add testing user
    mongo localhost:27017/dexdb --eval 'db.createUser({"user":"dexcell", "pwd":"runTestsHere", "roles": ["readWrite"]});'

SHELL


Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = "ubuntu/trusty64"

  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
  end

  config.vm.define "mongodb" do |mongodb|
    mongodb.vm.box="ubuntu/trusty64"
    mongodb.vm.network :private_network, ip: "192.168.33.10"
    mongodb.vm.provision :shell, inline: $bootstrap_mongo
    mongodb.vm.hostname = "mongodb"
    mongodb.vm.provider "virtualbox" do |v|
            v.memory = 1024
      v.cpus = 2
    end
  end

  config.vm.define "redis" do |redis|
    redis.vm.box= "ubuntu/trusty64"
    redis.vm.provision :shell, inline: $bootstrap_redis
    redis.vm.network :private_network, ip: "192.168.33.11"
    redis.vm.hostname = "redis"
    redis.vm.provider "virtualbox" do |v|
            v.memory = 1024
      v.cpus = 2
    end
  end

end
