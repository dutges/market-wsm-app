from __future__ import absolute_import

from celery_engine.celery_app import app as client


class Client():
    @staticmethod
    def enqueue(task, args=[], kargs={}):
        client.send_task(task, args=args, kwargs=kargs)