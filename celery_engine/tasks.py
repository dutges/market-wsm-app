
from worker.calculator import Calculator

from celery_app import app as worker
from client import Client


@worker.task
def calculate_price(dep_id, price_id, from_date, to_date, counter,type):
    calc = Calculator()
    status = calc.execution(dep_id, price_id, from_date, to_date,type)
    if status == "bad" and counter > 0:
        Client.enqueue("tasks.calculate_price", args=[dep_id, price_id, from_date, to_date, counter - 1, type])



