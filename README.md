# WSM_APP #

This application developed by Dexma team allows the users to configure and get their prices from the spanish free energy market.

It's really easy to use!

just create a new price with the parameters that you need and the formula that you use for it and in less than one minute you will be able to begin the importation of your free energetic price into Dexcell Energy Management, so easy and fast!


## Composition of the project ##

In this repository it's possible to distinguish three projects that together allows wsm app to be a web application with a concurrent task executer. This three parts are the webapp, the puller, and the worker.

To support the three projects the repository contains two libraries created by Dexma, the celery engine and the driver management.

### Webapp ###

here it's possible to find all the code that shapes what the user is able to see and configure from his browser.

### Puller ###

This is the trigger for the workers, so his main function is to say to them every day to calculate the prices for the next day to each client that has a price configurated in the application.

But thats not all! as the prices in the market are not static and can be modified the next month (to be consolidated) the puller has to trigger another job that is to see if the prices for each parameter in the previous month had been modificated and consolidated in which case the puller will give make work a little harder his workers and make them to recalculate all the prices for each client from the past month and reinsert them to Dexcell to assure that the prices are always coherent with the one's in the free market.

to be able to charge with so many tasks to his workers first the puller has to get from the free market api the prices from the next day and the past month and save them in a database (in this case MongoDB was the choosen one).

### Worker ###

This is the part that has the bigger charge in all the project, as is the one that for each price configured in the application has calculate his price and insert it in an hourly frequence to Dexcell.

To do that the worker for every price first get from the database the data from each parameter used in it (setted in the configuration via the web application) for the days selected, in the more used case (as it's launched every day automatically) for the next day and with them and using the formula provided via the configuration too calculates for each hour the resultant price, once is finished with all the calculations save them to Dexcell via his API to let the user enjoy his new prices in all kind of visualizations via the Dexcell platform.

### Celery_engine ###

This library is shared between the three projects as it's the one that configures celery and allows it to work properly.

### Driver management ###

This is the point that use all the application to acces external dependencies, as the databases used (redis, mongodb) or even the acces to http.

## Tecnology Support ##

### Language ###

All the application has been developed with the python language under the rules PEP8 for the formatting style

For further information (language): https://www.python.org/
For further information (PEP8): https://www.python.org/dev/peps/pep-0008/

### Databases ###

MongoDB - a non relational database documented-oriented used in our case to save all the hourly values for each parameter configurable in spanish free energetic market and the configurations from each client, as gives really nice performance to read big documents.

For further information: https://www.mongodb.com/

Redis - a non relational database, in-memory data structure store, used as cache to save all the tokens to access the Dexcell API, the best choice for small information and fast access to it. Used to as queue distributor by celery.

For further information: http://redis.io/

### Distributed Task Queue ###

To allow concurrency to the application, we use celery that is a task queue focused in real-time processing, so allows to enqueue all the tasks in a queue distributor or database, in our case redis, and be read by as much workers as we want.

For further information: http://celery.readthedocs.org/en/latest/

## Installation ##

Really easy to install!

Just clone it from git using the clone button in the top and download it where you want.

Once installed you don't need to worry about any kind of databases dependencies! just install vagrant (https://www.vagrantup.com/) that allows to create portable development environmnets and inside the folder of the application via the console execute **vagrant up**, now you have a new virtual machine up with an environment with all the dependencies already installed! amazing right!?

To make it work you just need two steps more, the first one is to select your configurations folders, where you will put the files .ini.example that you can find in any of the three projects, for this you just create a folder called dexma in /etc and put inside the three files without the extention .example

The final step will be install the python dependencies for the project written in the requirements.txt in the main folder of the project, if you are not used to use python I recommend you to install first via apt-get or aptitude if you prefer pip (the recommended tool for installing Python packages) and once installed go to your project and just execute the next command 

**pip install -r requirements.txt**, if you install them without using a virtualenv rememberthat you will need root privileges.

Now you are ready to execute any of the three projects!!!

For the puller just execute inside it python daily_routine.py or python monthly_routine.py

For the webapp just execute inside it python wsm.py

The worker is a little more problematic, but thanks to our help will be easy for you too, just go on the main folder of the project and having all the python dependecies installed as you will be able to use the celery command just type celery worker --app celery_engine.celery_app -c 1 (the -c 1 indicates that just one worker will be launched, without that condition by default celery creates as much workers as CPU's your computer has)


And that's all what you need to know to be able to use WSM_APP by Dexcell!

But if there are something that it's not clear don't be afraid to contact us via our mail support@dexmatech.com!
